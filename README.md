# README #

This ROS package creates 2.5D occupancy gridmap from Velodyne point cloud.

Input is Velodyne topic:

```
/velodyne_points
```

Output is 2.5D occupancy gridmap, and scan data:
```
/occmap/grid_map
/occmap/scan
```

The gridmap data is used for visualization in rviz, while the scan data is used for obstacle avoidance by the navigation module.
Scan data is created continuously each time step from velodyne point accounting the height constraints which are considered to be dangerous according to the vehicle dimensions. Therefore, obstacles taller than the upper limit and obstacles shorter than the lower limit will not be included in the scan data. Currently, height constraints are set as 0.2 m for low limit, and 3.0 m for high limit, with respect to the z-axis of the odom coordinate frame.	Make sure you made a correct transformation from the velodyne to base_link coordinate frame in lasertransform.launch.

- Current status:
  - [x] gridmap resolution is a parameter (see startoccupancygridmap.launch file), default is 0.1 m cell size 
  - [x] map size is a parameter (see startoccupancygridmap.launch file), default is 40 x 40 in number of meters
  - [x] height constraints are parameters (see startoccupancygridmap.launch file), default is offset_low=0.2 m, offset_high=3 m
  - [x] filtering the readings of the vehicle set as angular parameters (see startoccupancygridmap.launch file), default is angle_z_low=-180 deg, angle_z_high=180 deg around z axis while around xy plane is angle_xy_low=-31 deg which means no filtering
  - [x] filtering the readings of the vehicle set as bounding box in the local vehicle frame (see startoccupancygridmap.launch file), filter_x_min, filter_x_max, filter_y_min, filter_y_max


## Setup
```
sudo apt-get install ros-kinetic-grid-map-rviz-plugin
```

## Testing with rosbag

Download the bag file (unzip it) from [here](https://ferhr-my.sharepoint.com/:u:/g/personal/mseder_fer_hr/EZa5lbVp9UpLokMktxPe-BkBmg416vIPBoSVCWvDuLnIpg?e=d5fqVy)
```
terminal 1: roscore
terminal 2: rosparam set use_sim_time true
terminal 2: rosbag play --clock pokus_mapa2.bag
terminal 3: rosrun rviz rviz (add visualization for gridmap)
terminal 4: roslaunch occupancy_gridmap lasertransform.launch
terminal 5: roslaunch occupancy_gridmap startoccupancygridmap.launch
```
To visualize also the velodyne data transformed into base_link include the poincloud topic in rviz and start:
```
terminal 6: roslaunch occupancy_gridmap velodynetransform.launch
```

## Testing with real Husky

After starting the robot, start the following:

```
terminal 1: rosrun rviz rviz 
```
Add visualization for gridmap.
```
terminal 2: roslaunch occupancy_gridmap lasertransform.launch
```
This starts the static transform of the generated scan data to base_link.
```
terminal 3: roslaunch occupancy_gridmap startoccupancygridmap.launch
```
Launch the rhc navigation with parameters for odom as world frame, scan topic as /grid_map_simple_demo/scan

`roslaunch movingobstaclesrhc startmovinghusky.launch`



